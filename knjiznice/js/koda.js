
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var joinedDataCache;
var grafDia, grafSis, grafO2;

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

var defaultData = {
    first: {
        ehrId: "7b98b134-5bce-48fa-8422-14c2cf858228",
        data: {
            firstNames: "Grga",
		    lastNames: "Pitic",
	        dateOfBirth: "1952-7-5T11:20",
	        gender: "MALE",
	        partyAdditionalInfo: []
        }
    },
    
    second: {
        ehrId: "ec61ad48-8035-4978-9f86-9187116b8d4a",
        data: {
            firstNames: "Josko",
		    lastNames: "Mackov",
	        dateOfBirth: "1977-6-1T12:57",
	        gender: "MALE",
	        partyAdditionalInfo: []
        },
    },
    
    third: {
        ehrId: "c694b3c5-0258-4dc1-a2a2-e396196b8438",
        data: {
            firstNames: "Micka",
		    lastNames: "Erjavc",
	        dateOfBirth: "1960-11-23T7:00",
	        gender: "FEMALE",
	        partyAdditionalInfo: []
        }
    }    
};

//objekt privzetih vrednosti katere se ob generiranju novih podatkov spremenijo za +-5%
var defaultVitals = [{
            datumInUra: "2016-05-08T11:20Z",
            telesnaVisina: 182,
            telesnaTeza: 105,
            telesnaTemperatura: 36,
            sistolicniKrvniTlak: 118,
            diastolicniKrvniTlak: 92,
            nasicenostKrviSKisikom: 98,
            merilec: "Tanja Mikuz"
        },
        {
            datumInUra: "2016-05-09T11:40Z",
            telesnaVisina: 182,
            telesnaTeza: 105,
            telesnaTemperatura: 36,
            sistolicniKrvniTlak: 120,
            diastolicniKrvniTlak: 98,
            nasicenostKrviSKisikom: 99,
            merilec: "Tanja Mikuz"
        },
        {
            datumInUra: "2016-05-10T11:55Z",
            telesnaVisina: 182,
            telesnaTeza: 105,
            telesnaTemperatura: 36,
            sistolicniKrvniTlak: 110,
            diastolicniKrvniTlak: 90,
            nasicenostKrviSKisikom: 97,
            merilec: "Tanja Mikuz"
        },
        {
            datumInUra: "2016-06-08T10:40Z",
            telesnaVisina: 182,
            telesnaTeza: 105,
            telesnaTemperatura: 36,
            sistolicniKrvniTlak: 110,
            diastolicniKrvniTlak: 80,
            nasicenostKrviSKisikom: 95,
            merilec: "Tanja Mikuz"
        },
        {
            datumInUra: "2016-06-09T09:50Z",
            telesnaVisina: 182,
            telesnaTeza: 105,
            telesnaTemperatura: 36,
            sistolicniKrvniTlak: 106,
            diastolicniKrvniTlak: 80,
            nasicenostKrviSKisikom: 98,
            merilec: "Tanja Mikuz"
        },
        {
            datumInUra: "2016-06-10T11:10Z",
            telesnaVisina: 182,
            telesnaTeza: 105,
            telesnaTemperatura: 36,
            sistolicniKrvniTlak: 102,
            diastolicniKrvniTlak: 75,
            nasicenostKrviSKisikom: 99,
            merilec: "Tanja Mikuz"
        }];

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
    var ehrId = "";
    var sessionId = getSessionId();
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
    var partyData;
    
    if(stPacienta == 1) partyData = defaultData.first;
    else if(stPacienta == 2) partyData = defaultData.second;
    else partyData = defaultData.third;
    
    //zgenerira uporabnika
    $.ajax({
        url: baseUrl + "/ehr",
        type: "POST",
        async: false,
        success: function(data) {
            ehrId = data.ehrId;
            partyData.data.partyAdditionalInfo.push({key: "ehrId", value: ehrId});
            $("#preberiSporocilo").append("<div class='obvestilo label label-success fade-in'>Generiran bolnik '" + 
            partyData.data.firstNames + " " + partyData.data.lastNames + "', z EHR ID '" + ehrId + "'.</div><br/>")
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: "POST",
                contentType: 'application/json',
                data: JSON.stringify(partyData.data),
                async: false,
                success: function(response) {
                    if (response.action == 'CREATE') {
                        var rndMult = (Math.random() * 0.2) + 0.9;
                        defaultVitals.forEach(function(e) {
                            var podatki = {
                                "ctx/language": "en",
                                "ctx/territory": "SI",
                                "ctx/time": e.datumInUra,
                                "vital_signs/height_length/any_event/body_height_length": Math.floor(e.telesnaVisina * rndMult),
                                "vital_signs/body_weight/any_event/body_weight": Math.floor(e.telesnaTeza * rndMult),
                                "vital_signs/body_temperature/any_event/temperature|magnitude": Math.floor(e.telesnaTemperatura * ((Math.random() * 0.2) + 0.9)),
                                "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                                "vital_signs/blood_pressure/any_event/systolic": Math.floor(e.sistolicniKrvniTlak * ((Math.random() * 0.2) + 0.9)),
                                "vital_signs/blood_pressure/any_event/diastolic": Math.floor(e.diastolicniKrvniTlak * ((Math.random() * 0.2) + 0.9)),
                                "vital_signs/indirect_oximetry:0/spo2|numerator": Math.floor(e.nasicenostKrviSKisikom * ((Math.random() * 0.2) + 0.9))
                            };
                            var parametriZahteve = {
                                ehrId: ehrId,
                                templateId: 'Vital Signs',
                                format: 'FLAT',
                                committer: e.merilec
                            };
                            $.ajaxSetup({
        	                    headers: {"Ehr-Session": sessionId}
        	                });
                            $.ajax({
                               url: baseUrl + "/composition?" + $.param(parametriZahteve),
                               type: "POST",
                               contentType: 'application/json',
                               data: JSON.stringify(podatki),
                               async: false,
                               success: function (response) {
                                   
                               }
                            });
                        });
                    }
               },
               error: function(response) {
                   console.log(response);
                }
            });
        }
    });
    console.log(ehrId);
    return ehrId;
}

/**
 *  Pridobi podatke iz ARSO-vega arhiva za dolocen datum in oblikuje v JS objekt ki je parameter callback funkcije
 **/
function pridobiArsoPodatke(datum, object, callback) {
    //gremo cez crossorigin.me ker se iz https povezave povezujemo na http
    var url = 'https://cors-anywhere.herokuapp.com/http://meteo.arso.gov.si/webmet/archive/data.xml?lang=si&vars=12,15,18&group=halfhourlyData0&type=halfhourly&id=1828&d1=' + datum + '&nocache=0';
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function() {
        if(xhr.readyState === 4 && xhr.status === 200) {
            var data = xhr.responseText;
            var start = data.indexOf("_1828:");
            var end = data.indexOf("}}})]]>");
            data = data.substr(start - 1, end - start + 3);
            data = data.replace(new RegExp(/(_([0-9])+)|(p([0-9]))/, 'g'), function(s){
                return '"' + s + '"';
            });
            callback(data, object);
        }
    };
    xhr.setRequestHeader("Content-type", "text/xml")
    xhr.send();
}

var defaultGraphOptions = {
	scales: {
		yAxes: [{
			position: "left",
			"id": "y-axis-human",
			ticks: {
				beginAtZero: false
			}
		},
		{
			position: "right",
			"id": "y-axis-weather",
			ticks: {
				beginAtZero: false
			}
		}]
	},
	animation: {
		duration: 0
	},
	maintainAspectRatio: false
};

function posodobiGrafe() {
    
    if(grafDia != undefined) grafDia.destroy();
    if(grafSis != undefined) grafSis.destroy();
    if(grafO2 != undefined) grafO2.destroy();
    
    var timeLabels = [];
    var diastolicD = [];
    var sistolicD = [];
    var spO2D = [];
    var pressure = [];
    
    for(var i = joinedDataCache.length - 1; i >= 0; i--) {
        timeLabels.push(joinedDataCache[i].time.substr(0, 10));
        diastolicD.push(joinedDataCache[i].diastolicniKrvniTlak);
        sistolicD.push(joinedDataCache[i].sistolicniKrvniTlak);
        spO2D.push(joinedDataCache[i].nasicenostKrviSKisikom);
        if(joinedDataCache[i].tlak) pressure.push(joinedDataCache[i].tlak);
    }
    
    var diaData = {
    	labels: timeLabels,
    	datasets: [{
    		label: 'Dia. tl.',
    		yAxisID: "y-axis-human",
    		data: diastolicD,
    		fill: true,
    		borderColor: "#F44336",
    		backgroundColor: "rgba(244, 67, 54, 0.5)" 
    	},
    	{
    		label: 'Zra. tl.',
    		yAxisID: "y-axis-weather",
    		data: pressure,
    		fill: true,
    		borderColor: "#2962FF",
    		backgroundColor: "rgba(41, 98, 255, 0.5)" 
    	}]
    };
    
    grafDia = new Chart(document.getElementById("diaGraf"), {
        type: 'bar',
        data: diaData,
        options: defaultGraphOptions
    });
    
    var sisData = {
    	labels: timeLabels,
    	datasets: [{
    		label: 'Sis. tl.',
    		yAxisID: "y-axis-human",
    		data: sistolicD,
    		fill: true,
    		borderColor: "#F44336",
    		backgroundColor: "rgba(244, 67, 54, 0.5)" 
    	},
    	{
    		label: 'Zra. tl.',
    		yAxisID: "y-axis-weather",
    		data: pressure,
    		fill: true,
    		borderColor: "#2962FF",
    		backgroundColor: "rgba(41, 98, 255, 0.5)" 
    	}]
    };
    
    grafSis = new Chart(document.getElementById("sisGraf"), {
        type: 'bar',
        data: sisData,
        options: defaultGraphOptions
    });
    
    var o2Data = {
    	labels: timeLabels,
    	datasets: [{
    		label: 'Nas. z O2',
    		yAxisID: "y-axis-human",
    		data: spO2D,
    		fill: true,
    		borderColor: "#F44336",
    		backgroundColor: "rgba(244, 67, 54, 0.5)" 
    	},
    	{
    		label: 'Zra. tl.',
    		yAxisID: "y-axis-weather",
    		data: pressure,
    		fill: true,
    		borderColor: "#2962FF",
    		backgroundColor: "rgba(41, 98, 255, 0.5)" 
    	}]
    };
    
    grafO2 = new Chart(document.getElementById("o2Graf"), {
        type: 'bar',
        data: o2Data,
        options: defaultGraphOptions
    });
    
}

function narisiGrafe() {
    for(var i = 0; i < joinedDataCache.length; i++) {
        var datum = joinedDataCache[i].time.substr(0, 10);
        var cas = joinedDataCache[i].time.substr(11, 5);
        var h = parseInt(cas.substr(0, 2));
        var m = parseInt(cas.substr(3, 2));
        var iskan = h * 2 + (m > 30 ? 1:0);
        pridobiArsoPodatke(datum, joinedDataCache[i], function(data, object) {
            var obj = JSON.parse(data);
            var n = 0;
            for(var key in obj._1828) {
                if(n == iskan) {
                    object.tlak = obj._1828[key].p0;
                    object.temp = obj._1828[key].p1;
                    object.vlaga = obj._1828[key].p2;
                    break;
                }
                n++;
            }
            posodobiGrafe();
        });
    }
}

//prebere podathe iz EHR
function preberiEHR() {
    var ehrId = $("#preberiEHRid").val();
    var sessionId = getSessionId();
    
    $("#prikazIzbrane").html("");
    $("#izbranaMeritev").html("");
    
    if(grafDia != undefined) grafDia.destroy();
    if(grafSis != undefined) grafSis.destroy();
    if(grafO2 != undefined) grafO2.destroy();
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	var joined = [];
	$.ajax({
        url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
        type: "GET",
        success: function(data) {
            var party = data.party;
            $("#preberiSporocilo").html("<span class='obvestilo label label-success fade-in'>Bolnik '" + 
            party.firstNames + " " + party.lastNames + "', ki se je rodil '" + party.dateOfBirth + "'.</span>");
            $.ajax({
               url: baseUrl + "/view/" + ehrId + "/blood_pressure",
               type: "GET",
               success: function(blood_pressure) {
                   blood_pressure.forEach(function(e, index) {
                       var j = {
                           time: e.time,
                           sistolicniKrvniTlak: e.systolic,
                           diastolicniKrvniTlak: e.diastolic
                       }
                       joined[index] = j;
                   });
                    $.ajax({
                       url: baseUrl + "/view/" + ehrId + "/spO2",
                       type: "GET",
                       success: function(spO2) {
                           for(var i = 0; i < spO2.length; i++) {
                               for(var j = 0; j < joined.length; j++) {
                                   if(spO2[i].time === joined[j].time) {
                                       joined[j].nasicenostKrviSKisikom = spO2[i].spO2;
                                       break;
                                    }
                                }
                            }
                            joinedDataCache = joined;
                            
                            $("#prikazMeritev").html("");
                            var times = '<table class="table table-striped table-hover"><tr><th>Čas</th></tr>';
                            for(var i = 0; i < joined.length; i++) {
                                times = times + '<tr onclick="showData(\'' + joined[i].time + '\')"><td>' + joined[i].time + '</td></tr>';
                            }
                            times = times + '<table>';
                            $("#prikazMeritev").html(times);
                            narisiGrafe();
                       },
                       error: function(err) {
                           $("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
                       }
                    });
               },
               error: function(err) {
                   $("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
               }
            });
        },
        error: function(err) {
           $("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
        }
    });
}

function showData(dateArg) {
    for(var i = 0; i < joinedDataCache.length; i++) {
        if(joinedDataCache[i].time === dateArg) {
            var podatki = '<table class="table table-striped table-hover"><tr><th>Meritev</th><th class="text-right">Vrednost</th></tr>';
            podatki += '<tr><td>Sistolični krvni tlak</td><td class="text-right">' + joinedDataCache[i].sistolicniKrvniTlak + ' mm[Hg]</td></tr>';
            podatki += '<tr><td>Diastolični krvni tlak</td><td class="text-right">' + joinedDataCache[i].diastolicniKrvniTlak + ' mm[Hg]</td></tr>';
            podatki += '<tr><td>Nasičenost krvi s kisikom</td><td class="text-right">' + joinedDataCache[i].nasicenostKrviSKisikom + '%</td></tr>';
            podatki += '</table>'
            $("#prikazIzbrane").html(podatki);
            $("#izbranaMeritev").html(" " + dateArg);
            return;
        }
    }
}

$(document).ready(function() {
    $("#menujska-vrstica ul li:eq(2) a").click(function() {
        var id1 = generirajPodatke(1);
        var id2 = generirajPodatke(2);
        var id3 = generirajPodatke(3);
        $("#preberiObstojeciEHR").html("<option value=\"\"></option><option value=" + id1 + ">Grga Pitic</option><option value=" + id2 + ">Josko Mackov</option><option value=" + id3 + ">Micka Erjavc</option>");
    });
    
    $('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
});